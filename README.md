# Report on Motorbike Ambulance Calls

Otiginal report: <https://hackmd.io/@banderlog/B1F1XcTcS>  
Matplotlib cool comparsion plot function: <https://hackmd.io/@banderlog/rJDjp1Rcr>

## Introduction

My task was to build regression model to the given dataset "Motobike Ambulance Calls".
The model needs to predict the hourly ambulance calls count based on the environmental and seasonal settings.
Also, model should provide monotonic in terms of some features, if it is proved by data.

## Materials and methods

All work was done on Ubuntu 18.04 machine with CPU Core i9-7900X and 32 GB RAM. I used python-3.6.8 with following packages:

+ pandas-0.22.0
+ scipy-1.2.1
+ sklearn-0.21.0
+ statsmodels-0.10.1
+ matplotlib-3.0.2
+ seaborn-0.9.0
+ xgboost-0.90

Dataset was provided by SoftServe.

I got approximately 8 hours for this task, which had not included machine time that has been taken on model hyperparameters cross-validation.

## Data description and outliers detection

It is theorized that motorbike accidents and corresponded ambulance calls (`cnt`) are highly depended on environmental and seasonal settings,
like weather conditions, precipitation, day of the week, season, an hour of the day, etc.
The data were aggregated over the course of two years, on an hourly basis, and then extracted and extended with the corresponding weather and seasonal information.

Firstly I checked if the dataset has NA's, it had not, so I built the correlation matrix:

![](https://i.imgur.com/PhLJCcI.png)

As we may see in the figure above, dataset had no features with good correlation with `cnt` 
( $`>|0.5|`$, but there are two features with a high positive correlation between them  -- `temp` and `atemp` (r=0.987).
No surprise in that -- `temp` is a main basis for `atemp` calculation, probably they are collinear.

Next, I got a brief dataset description:

![](https://i.imgur.com/fWj7uXU.png)

And one thing trapped into my eye -- high standard deviation of `cnt`, so I built boxplots of all quantitative features:

![](https://i.imgur.com/O1YPHyg.png)

As we may see, `windspeed`, `humidity` and `cnt` are, presumably, from skewed  datasets (not from a normal distribution).

As long as `cnt` was my target feature, I built its distribution and quantile-quantile plot:

![](https://i.imgur.com/7OTqRt0.png)

On the last figure it is clear that we have a distribution with the positive skew, which is confirmed by medcouple (MC=0.25352113).

I decided to use an interquartile range (IQR) for outliers detection, but it needs to be adjusted for skewed distribution outliers must be adjusted (standard $`1.5*IQR`$  will not suffice).
Thus I had to use calculated MC value to adjust IQR  accordingly[^adjbox]:

```math
\begin{cases}
[Q_1 − 1.5e^{(−4MC)}IQR; Q_3 + 1.5e^{3MC}IQR], if MC \geq 0 \\
[Q_1 − 1.5e^{(−3MC)}IQR; Q_3 + 1.5e^{4MC}IQR], if MC < 0
\end{cases}
```

[^adjbox]: Hubert, M.; Vandervieren, E. (2008). "An adjusted boxplot for skewed distribution".
Computational Statistics and Data Analysis. 52 (12): 5186–5201. CiteSeerX 10.1.1.90.9812. doi:10.1016/j.csda.2007.11.008.

But there are no outliers in `cnt` according to corrected interval ($`[-91.13; 1054.42]`$).


![](https://i.imgur.com/rs0Kkg0.png)

First 4 plots on the figure above shows the relationship between the `cnt` and quantitative features.
As we may see, `windspeed` is also positively skewed, others are more-or-less normal.
All of them are nonlinear nonmonotonic and have outliers.  

Next 7 plots are barplots which are showing how many `cnt` were made in qualitative features groups.
We could see that there less `cnt` in spring, but month distribution tells us another story:
spring month should have numbers 3, 4, 5 but the fewest `cnt` are in 1, 2, 12, which are winter.
This is odd.

Hour plot is interesting, we may suppose that most of `cnt` were made in traffic jams time[^jams] and very few were made at deep night.
Also, most `cnt` happened in working days.

The last plot is the distribution of `cnt` with date relation, it looks somewhat normal, as expected.

Thre is no graph for that, but `cnt` sum from 2012 year is 1.67 times more than from 2011 (2049576 and 1243103 accordingly).
And I do not know why.

We may see that most `cnt` were made in normal weather conditions when there were no extreme temperatures or conditions like strong wind or heavy rain,
which could imply that bikers prefer not to ride at such times.

[^jams]: https://mobilitylab.org/2018/02/06/u-s-is-the-world-leader-in-traffic-jams/


## Data preparation and training

I removed `yr`, `date`, `season`, `atemp` features. The `yr` and `date` because I wanted to generalize this data over the years.
The `season` because it is surplus, `mnth` contains all necessary info.
And `atemp` is collinear to `temp`, also `atemp` calculation implies the usage of `temp`,`humidity` and `windspeed` (optionally)[^atemp].
Potentially, we could get rid of `temp`,`humidity` and `windspeed` but we do not know how `atemp` was calculated in this dataset,
so it is safer to get rid of it instead  (I tried opposite variant and achieved worser results). 

[^atemp]: https://en.wikipedia.org/wiki/Apparent_temperature

Next, I randomly split the dataset to train and test subsets with 0.8 ratio. After which I made a grid search cross-validation to find the best hyperparameters.  
They were `{'alpha': 0.1, 'gamma': 0.2, 'max_depth': 8, 'learning_rate': 0.05}` (please refer to jupyter-notebook for details).

After, I trained XGBoostRegressor, here model train curves:

![](https://i.imgur.com/FwhwJ2k.png)

Feature importance plot:

![](https://i.imgur.com/cuTmu9n.png)

As I supposed before, `hr` is the most important feature, with followed `hum`, `temp`, `windspeed`.
I tried variants without the `workingday` and `holiday`, because I supposed that `mnth` and `weekday` include most of their info but did not prevail.

## Results

After Regression Tree training I received next accuracy metrics:

|                                | test   |
| ------------------------------ | -------|
| $`R^2`$                        | 0.89   |
| Explained Variance Score       | 0.89   |
| maximal residual error         | 484.67 |
| mean absolute error            | 39.84  |
| mean squared error             | 3635.7 |
| median absolute error          | 23.67  |


$`R^2`$ (coefficient of determination) and Explained Variance Score both are high (0.89), which means that the model describes the dataset pretty good.

Not robust to outliers maximal residual error (484.67) and mean squared error (3635.7) are high.
While robust to outliers mean absolute error and median absolute error are only 39.84 (`cnt` mean is 189) and 23.67 (`cnt` median is 142) respectively.

![](https://i.imgur.com/qO2oAEh.png)

Truth vs Predicted plot explains the above metrics. It has dense dot cloud along the line, which becomes more sparse at big rare `cnt` values. Outliers are few but with great differences.

## Discussion and future work 

I got a fine model which could be improved.

Usage of more robust to outliers loss function should be taken into consideration.
I forgot to remove outliers based on statistics of other features, e.g.: the `windspeed` has 2188 outliers after IQR adjustment, `hum` -- 892. This, potentially, could improve model accuracy.
Further hyperparameter tuning could improve final accuracy metrics.
